package handlers

import (
	"github.com/gofiber/fiber/v2"
	"goredis/services"
)

type catalogHandler struct {
	catalogService services.CatalogService
}

func NewCatalogHandler(catalogSrv services.CatalogService) CatalogHandler {
	return catalogHandler{catalogSrv}
}

func (handler catalogHandler) GetProducts(c *fiber.Ctx) error {
	products, err := handler.catalogService.GetProducts()
	if err != nil {
		return err
	}
	response := fiber.Map{
		"status":   "ok",
		"products": products,
	}
	return c.JSON(response)
}
