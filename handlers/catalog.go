package handlers

import "github.com/gofiber/fiber/v2"

type Product struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Quantity int    `json:"quantitym"`
}

type CatalogHandler interface {
	GetProducts(c *fiber.Ctx) error
}
