# Golang Redis
El proyecto muestra como el uso de una cache puede reducir los tiempos de acceso a datos

## Docker
El archivo docker-compose contiene los siguientes servicios
- k6: Permite ejecutar pruebas de carga
- Grafana: Para realizar la visualizacion de las metricas
- InfluxDB: Almacenar las metricas de los test
- MariaDB: almacena los datos de la app
- Redis: base de datos en memoria

## Iniciar el proyecto
Para ejecutar los servicios se ejecuta el comando
```bash
docker-compose up
```
Para poder ejecutar la aplicacion debe correr el comando
```bash
go run .
```
Lo cual iniciara una aplicacion en el puerto 8080


## Ejecutar pruebas
Para ejecutar las pruebas debemos correr el siguiente comando en la terminal
```bash
docker-compose run k6 /scripts/test.js
```
Si queremos cambiar las pruebas debemos cambiar el archivo anteriormente mencionado "test.js"


## Grafana
Se debe agregar dashboard para k6